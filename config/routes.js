/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

module.exports.routes = {

  /***************************************************************************
  *                                                                          *
  * Make the view located at `views/homepage.ejs` your home page.            *
  *                                                                          *
  * (Alternatively, remove this and add an `index.html` file in your         *
  * `assets` directory)                                                      *
  *                                                                          *
  ***************************************************************************/
  //root path
  '/': { view: 'login' },
  'GET /pages/homepage': {action: 'users/homepage'},
  'GET /pages/login': {action: 'users/login'},
  //articles routes
   'GET /articles':    { action: 'articles/index' },
   'GET /articles/new': { action: 'articles/new'},
   'POST /articles/create': { action: 'articles/create'},
   'GET /articles/show/:id':    { action: 'articles/show' },
   'GET /articles/edit/:id':    { action: 'articles/edit' },
   'POST /articles/update/:id': { action: 'articles/update'},
   'POST /articles/destroy/:id': { action: 'articles/destroy'},
   //users routes
    'GET /users':    { action: 'users/index' },
    'GET /users/new': { action: 'users/new'},
    'POST /users/create': { action: 'users/create'},
    'GET /users/show/:id':    { action: 'users/show' },
    'GET /users/edit/:id':    { action: 'users/edit' },
    'POST /users/update/:id': { action: 'users/update'},
    'POST /users/destroy/:id': { action: 'users/destroy'},

    'GET /users/new_session': { action: 'users/new_session'},
    //'GET /users/logout': { action: 'users/logout'},


    //ROTAS PARA O LOGIN
    'GET /login': { view: 'login' },
    'POST /login': 'AuthController.login',
    '/logout': 'AuthController.logout',
    'GET /register': { view: 'register' },

    //ROTA PARA AS NOVAS VIEWS USER
    'POST /user/create' : {action: 'user/create'},
    'GET /user':    { action: 'user/index' },

    //VERIFICA SE JA ESTA LOGADO
    //  '/': [
    //    { policy: 'check_login' },
    //    { view: 'homepage' }
    //  ],

    // 'get /login': [
    //    { policy: 'check_login' },
    //    { view: 'login' }
    //  ]



  /***************************************************************************
  *                                                                          *
  * More custom routes here...                                               *
  * (See https://sailsjs.com/config/routes for examples.)                    *
  *                                                                          *
  * If a request to a URL doesn't match any of the routes in this file, it   *
  * is matched against "shadow routes" (e.g. blueprint routes).  If it does  *
  * not match any of those, it is matched against static assets.             *
  *                                                                          *
  ***************************************************************************/


};
