module.exports = function(req, res, next) {
  sails.log('COMEÇA AQUI O LOG----------------------------');
  console.log(req.session.User);
  sails.log('ACABA AQUI----------------------------');

    if (!req.session.User) {
        return res.view('pages/login');
    }

    return next();
};
