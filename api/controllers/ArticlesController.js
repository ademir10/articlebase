/**
 * ArticlesController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {

  index:function(req, res) {
    Articles.find({}).exec(function(err,articles){
      if(err) {
        res.send(500, {error: 'Database error'});
      }
      res.view('articles/index', {articles:articles});
    });
  },

  new:function(req, res) {
    res.view('articles/new');
  },

  create: async function(req, res) {
    var title = req.body.title;
    var body = req.body.body;

    var createdUser = await Articles.create({title: title, body: body}).fetch();
    res.redirect('/articles/show/' + createdUser.id);
  },

  show:function(req,res) {
    Articles.findOne({id: req.params.id}).exec(function(err, article) {
      if(err) {
        res.send(500, {error: 'Database error'});
      }
      res.view('articles/show', {article:article});
    });
  },

  edit:function(req,res) {
    Articles.findOne({id: req.params.id}).exec(function(err, article) {
      if(err) {
        res.send(500, {error: 'Database error'});
      }

      res.view('articles/edit', {article:article});
    });
  },

  update: function(req, res) {

    var title = req.body.title;
    var body = req.body.body;

    Articles.update({id: req.params.id},{title: title, body: body}).exec(function(err){
    if(err) {
      res.send(500, {error: 'Database error'});
    }
    res.redirect('/articles');
    });

    return false;


  },

  destroy: async function(req,res) {
    Articles.destroy({id:req.params.id}).exec(function(err){
      if(err) {
        res.send(500, {error: 'Database error'});
      }
      res.redirect('/articles');
      });
      return false;
  }

};
