/**
 * UserController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {

  create: function(req, res) {
    var paramObj = {
      username: req.param('username'),
      email: req.param('email'),
      password: req.param('password'),
    }

    // Create a User with the params sent from
    // the sign-up form --> new.ejs
    User.create(paramObj, function usersCreated(err, users) {
      if (err) {
        console.log(err);
        req.session.flash = {
          err: err
        }
        return res.redirect('/user/new');
      }
      // res.json(users);
      res.view('user/show/');
    });
  },

  index: function (req, res) {
    User.find( function (err, users) {
      if(err) {
        console.log(err);
      }
      res.view({users:users});
    });
  }


};
