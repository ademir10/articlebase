/**
 * UsersController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {

  //kill session logout
  logout: function(req, res) {
        // clear login sessions here
        req.session.destroy(function(err) {
          setTimeout(function(){
            return res.view('pages/login', {icon: 'success', title: 'See you soon!'});
          }, 1000); // redirect wait time 2.5 seconds
        });
      },

  //store user session after login
  new_session: function(req, res) {
  Users.findOne({
  email: req.param('email')
})
.then(function (user){
  if(!user) {
    return res.view('pages/login', {icon: 'warning', title: 'Dados inválidos, verifique os dados.'});
  }
    //guardando a sessão do user
    req.session.authenticated = true;
    req.session.User = user;
      return res.view('pages/homepage', {icon: 'success', title: 'Bem vindo ' + req.session.User['nome'] + '!'});
  })
  // If there was some kind of usage / validation error
  .catch({ name: 'UsageError' }, function (err) {
    return res.badRequest(err);
  })
  // If something completely unexpected happened.
  .catch(function (err) {
    return res.serverError(err);
  });
},

  new: function(req,res){
    res.view();
  },

  create: function(req, res) {
    var paramObj = {
      nome: req.param('nome'),
      email: req.param('email'),
      password: req.param('password'),
    }

    // Create a User with the params sent from
    // the sign-up form --> new.ejs
    Users.create(paramObj, function usersCreated(err, users) {
      if (err) {
        console.log(err);
        req.session.flash = {
          err: err
        }
        return res.redirect('/users/new');
      }
      // res.json(users);
      res.redirect('/users/show/' + users.id);
    });
  },

  show: function(req, res, next) {
    Users.findOne(req.param('id'), function foundUsers(err, users) {
      if (err) return next(err);
      if (!users) return next();

      // res.json(users);
      res.view({
        users: users
      });
    });
  },

  index: function(req, res, next) {
    Users.find(function foundUserss(err, userss) {
      if (err) return next(err);

      res.view({
        userss: userss
      });
    });
  },

  edit: function(req, res, next) {

    Users.findOne(req.param('id'), function foundUsers(err, users) {
      if (err) return next(err);
      if (!users) return next('users doesn\'t exist.');

      res.view({
        users: users
      });
    });
  },

  update: function(req, res, next) {

    var paramObj = {

      nome: req.param('nome'),

      email: req.param('email'),

      password: req.param('password'),

    }

    Users.update(req.param('id'), paramObj, function usersUpdated(err) {
      if (err) {
        console.log(err);

        req.session.flash = {
          err: err
        }

        return res.redirect('/users/edit/' + req.param('id'));
      }

      res.redirect('/users/show/' + req.param('id'));
    });
  },

  destroy: function(req, res, next) {

    Users.findOne(req.param('id'), function foundUsers(err, users) {
      if (err) return next(err);

      if (!users) return next('Users doesn\'t exist.');

      Users.destroy(req.param('id'), function usersDestroyed(err) {
        if (err) return next(err);
    });

      res.redirect('/users');

    });
  }



};
